import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { checkAuth } from "./checkAuth";

interface WithAuthProps {}

const withAuth = <P extends object>(
  WrappedComponent: React.ComponentType<P>
) => {
  const WithAuth: React.FC<P & WithAuthProps> = (props) => {
    const router = useRouter();
    const authToken =
      typeof window !== "undefined" && localStorage.getItem("token");
    useEffect(() => {
      if (!authToken && router.pathname !== "/login") {
        router.push("/login");
        console.log("reached here-----");
        if (authToken && router.pathname.includes("/login")) {
          localStorage.removeItem("token");
        }
      } else if (
        authToken &&
        (router.pathname.includes("/login") ||
          router.pathname.includes("/register"))
      ) {
        console.log("reached here");
        router.push("/");
        localStorage.removeItem("token");
      } else {
        fetch("http://localhost:5000/users/user", {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        })
          .then((res) => {
            if (!res.ok) {
              throw new Error("Failed to verify token");
            }
          })
          .catch((err) => {
            console.error(err);
            router.push("/login");
          });
      }
    }, []);

    return <WrappedComponent {...(props as P)} />;
  };

  return WithAuth;
};

export default withAuth;
