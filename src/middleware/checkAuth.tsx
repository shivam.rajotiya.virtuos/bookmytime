import { NextPageContext } from "next";
import Router from "next/router";

export const checkAuth = (ctx: NextPageContext) => {
    const authToken = typeof window !== "undefined" && localStorage.getItem("token");

  // If there's no token, redirect to login page
  if (!authToken) {
    if (ctx.res) {
      ctx.res.writeHead(302, { Location: "/login" });
      ctx.res.end();
    } else {
      Router.push("/login");
    }
  }

  return { authToken };
};
