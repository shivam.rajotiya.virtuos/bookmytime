import { NextPage } from "next";

export type PageWithLayout = NextPage & {
  getLayout?: (page: JSX.Element) => JSX.Element;
};
