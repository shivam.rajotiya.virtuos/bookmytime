import Navbar from "@components/Bookings/Navbar/navbar";
import LabelBottomNavigation from "@components/BottomNavbar/bottom_fixed_navbar";
import Footer_home from "@components/Homepage/Footer/footer";

import { ReactNode } from "react";

type Props = {
  children: ReactNode;
};
function Layout({ children }: Props) {
  return (
    <div>
      <div className="navbar-main">
        <Navbar />
      </div>
      <main>{children}</main>

      <div className="bottom-nav">
        <LabelBottomNavigation />
      </div>
      <Footer_home />
    </div>
  );
}

export default Layout;
