import React from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import swal from "sweetalert";
import styles from '../../styles/Button.module.scss'
// import styles from '../../../scss-library/my-library.module.scss'
import cartdata from "./cartdata.json";
const Cart = () => {
  const services = cartdata.map((data) => {
    return data.service;
  });
  const price = cartdata.map((data) => {
    return data.price;
  });
  var prices = price.map(function (str) {
    return parseInt(str);
  });

  let cartvalue = prices.reduce(
    (accumulator, currentValue) => accumulator + currentValue
  );
  console.log(price);
  console.log(prices);

  console.log(cartvalue);
  const handleDelete = ()=>{
    swal({
      title: "Item removed from Cart",

      icon:"success",
    });

  }
  return (
    <div className="cart-main-container">
      <div className="cart-container">
        <div className="cart-header">
          <div className="cart-title">
            <h1>Services Selected</h1>
          </div>
          <div className="cart-header-cartvalue">
            <h1>{services.length}</h1>
          </div>
        </div>

        <div className="cart-body">
          {cartdata.map((data) => (
            <div key={data.id} className="cart-card">
              <div className="cart-card-service">
                <h5>Service : {data.service}</h5>
              </div>
              <div className="cart-card-comments">{data.message}</div>
              <div className="cart-card-footer">
                <div className="cart-price">${data.price}</div>
                <div className="cart-card-service">{data.date}</div>

                <div className="cart-card-time">
                  {/* <button className={styles.error}>button</button> */}
                  <button>{data.time}</button>
                  < DeleteIcon  
                  onClick={handleDelete}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className="checkout-button">
          <button>
            ${cartvalue}
            &nbsp; Checkout
          </button>
        </div>
      </div>
    </div>
  );
};

export default Cart;
