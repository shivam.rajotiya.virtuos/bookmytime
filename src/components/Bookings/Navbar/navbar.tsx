import Navbar_header from "@components/Homepage/Navbar/nav_header";
import React, { useEffect, useState } from "react";
import navlogo from "../../../pages/images/book-my-time-logo.svg";
import Avatar from "@mui/material/Avatar";
import SearchIcon from "@mui/icons-material/Search";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import Image from "next/image";
import Button from "@mui/material/Button";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { useRouter } from "next/router";

import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Sidemenu from "@components/SideMenu/side_menu";
import Search from "@mui/icons-material/Search";
import Link from "next/link";
import axios from "axios";
import LogoutButton from "@components/LogoutButton";
const Navbar = () => {
  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: "white",

    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  }));
  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
  }));
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: {
    currentTarget: React.SetStateAction<HTMLElement | null>;
  }) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const API_KEY = `957bb5eb1ba564bf054380d1044a528c`;
  const API = `https://api.openweathermap.org/data/2.5/weather?`;
  const [latitude, setLatitude] = useState<unknown>("");
  const [longitude, setLongitude] = useState<unknown>("");
  const [location, setLocation] = useState<string>("");

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setLatitude(position.coords.latitude);
      setLongitude(position.coords.longitude);

      async function fetchData() {
        let finalEndpoint = `${API}lat=${position.coords.latitude}&lon=${position.coords.longitude}&appid=${API_KEY}`;

        try {
          const response = await axios.get(finalEndpoint);
          console.log(response.data);
          setLocation(response.data.name);
        } catch (error) {
          console.log(error);
        }
      }

      fetchData();
    });
  }, []);
  const router = useRouter();

  const handleLogout = () => {
    localStorage.removeItem("token");
    router.push("/login");
  };

  return (
    <div className="booking-navbar-main-container">
      <div className="booking-navbar-container">
        <div className="ham-menu-bookings">
          <Sidemenu />
        </div>
        <div className="booking-searchbox">
          <div className="nav-image">
            <Image src={navlogo} alt="logo" />
          </div>

          {/* <div className="booking-search">
            <input type="text" placeholder="What are you looking for?" />
            <div className="booking-search-icon">
              <SearchIcon />
            </div>
          </div> */}
        </div>
        <div className="search-box-bookings">
          <Search style={{ borderRadius: "20px" }}>
            <SearchIconWrapper>
              <SearchIcon style={{ color: "#16252d" }} />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
        </div>
        <div className="navbar-right-section">
          <div className="location-navbar">
            <LocationOnIcon />
            <p>{location}</p>
          </div>
          {/* <p>{location}</p> */}
          <div className="booking-menus">
            <div className="booking-header-dropdowns">
              <Navbar_header />
            </div>
            <div className="booking-icons">
              {/* <ChatBubbleOutlineIcon /> */}
              <Link href={"/notifications"}>
                <NotificationsNoneIcon />
              </Link>
            </div>

            <div className="navbar-avatar" onClick={handleClick}>
              <Avatar
                alt="Remy Sharp"
                src="https://media.npr.org/assets/img/2022/11/08/ap22312071681283-0d9c328f69a7c7f15320e8750d6ea447532dff66.jpg"
              />
            </div>
          </div>
          <Menu
            style={{
              textDecoration: "none",
            }}
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <Link
              style={{
                textDecoration: "none",
                color: "#16252d",
              }}
              href={"/account"}
            >
              <MenuItem
                style={{ display: "flex", justifyContent: "center" }}
                onClick={handleClose}
              >
                Profile
              </MenuItem>
            </Link>
            {/* <MenuItem onClick={handleClose}>My account</MenuItem> */}
            {/* <MenuItem ><Link style={{color:"#000000DE"}} href={'/contactus'}>Contact Us  </Link></MenuItem> */}

            <MenuItem onClick={handleClose}>
              {/* <LogoutButton />
               */}
              <button className="logout-button" onClick={handleLogout}>
                Logout
              </button>
            </MenuItem>
          </Menu>
        </div>
      </div>
      <div className="search-box-bookings-mobile">
        <Search style={{ borderRadius: "20px" }}>
          <SearchIconWrapper>
            <SearchIcon style={{ color: "#16252d" }} />
          </SearchIconWrapper>
          <StyledInputBase
            placeholder="Search…"
            inputProps={{ "aria-label": "search" }}
          />
        </Search>
      </div>
    </div>
  );
};

export default Navbar;
