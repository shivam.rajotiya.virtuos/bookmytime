import React, { useState, useEffect } from "react";
import bookingdata from "./bookingdata.json";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import DeleteIcon from '@mui/icons-material/Delete';
import Avatar from "@mui/material/Avatar";
import ChatBubbleIcon from "@mui/icons-material/ChatBubble";
const Booking_cards = () => {
  // const [bookingData, setBookingData] = useState<any>([]);

  // useEffect(() => {
  //   fetch("bookingdata.json")
  //     .then((response) => response.json())
  //     .then((data) => setBookingData(data));
  // }, []);
  return (
    <>
      <div className="booking-cards-main-container">
        <Carousel
          //  showArrows={true}
          showThumbs={false}
          autoPlay={true}
          stopOnHover={true}
          infiniteLoop={true}
        >
          {bookingdata.map((booking) => (
            <div key={booking.id} className="booking-card">
              <div className="booking-card-name">
                <Avatar
                  alt="avatar"
                  src="https://media.npr.org/assets/img/2022/11/08/ap22312071681283-0d9c328f69a7c7f15320e8750d6ea447532dff66.jpg"
                />
                <h3> {booking.name}</h3>
              </div>
              <div className="booking-card-service">
                <h6>Required Services</h6>
                <h6>{booking.service}</h6>
              </div>
              <div className="booking-card-service">
                <h6>Appointment Date</h6>
                <h6>{booking.date}</h6>
              </div>
              <div className="booking-card-service">
                <h6>Booked for</h6>
                <h6>{booking.bookedfor}</h6>
              </div>
              <div className="booking-card-service">
                <h6>Appointment Time </h6>
                <h6> {booking.time}</h6>
              </div>
              <div className="booking-card-comments">
                <h6>Special Comments</h6>
                <h6>{booking.message}</h6>
              </div>
              <div className="booking-price">
                <h5>$999.00</h5>
              </div>
              <div className="booking-card-buttons">
              <button className="booking-card-decline"><DeleteIcon/></button>

                <button className="booking-card-approve">Approve</button>

                <button className="booking-card-message">
                  <ChatBubbleIcon />
                </button>
              </div>
            </div>
          ))}
        </Carousel>
      </div>
    </>
  );
};

export default Booking_cards;
