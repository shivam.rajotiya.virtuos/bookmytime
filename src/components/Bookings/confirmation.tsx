import { Dayjs } from "dayjs";
import React from "react";

type BookingFormProps = {
  date: Dayjs | null;
  timestart: Dayjs | null;
  timeend: Dayjs | null;
};
const Confirmation = ({ date, timestart, timeend }: BookingFormProps) => {
  return (
    <>
      <h3>Thank You</h3>
<h5>Booking Confirmed</h5>
      <p>
        Selected Date: {date ? date.format("YYYY-MM-DD") : "No date selected"}
      </p>
      <p>
        Time Slot: {timestart ? timestart.format("HH:mm") : "-"}
        {timeend ? timeend.format("HH:mm") : "-"}
      </p>
    </>
  );
};

export default Confirmation;
