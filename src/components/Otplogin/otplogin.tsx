import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import swal from "sweetalert";

import { useRouter } from "next/router";
import axios from "axios";
// import { PageWithLayout } from "../types/PageWithLayout";

const OtpLogin = () => {
  const [otptrue, setOtptrue] = useState(false);

  const router = useRouter();

  const [phoneNumber, setPhoneNumber] = useState("");
  const [verificationCode, setVerificationCode] = useState("");

  const [success, setSuccess] = useState<null>(null);
  const [error, setError] = useState<string>("");
  const [minutes, setMinutes] = useState<number>(1);
  const [seconds, setSeconds] = useState<number>(30);
  const formattedPhoneNumber = "+91" + phoneNumber;
  const handleSendOTP = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    try {
      const response = await axios.post("http://localhost:5000/auth/send-otp", {
        formattedPhoneNumber,
      });
      setSuccess(response.data.message);
      console.log(response);
      console.log("-----", setSuccess);

      if (response.data.error === "User not found") {
        setOtptrue(false);

        swal({
          title: "User not found",
          icon: "error",
        });

        setPhoneNumber("");
      } else {
        setOtptrue(true);
      }
    } catch (error) {
      console.log(error);
      setError("User not found");

      alert("User not found");
    }
    setMinutes(1);
    setSeconds(30);
  };

  const handleVerifyOTP = async (e: { preventDefault: () => void }) => {
    e.preventDefault();

    try {
      const response = await axios.post(
        "http://localhost:5000/auth/verify-otp",
        { formattedPhoneNumber, code: verificationCode }
      );
      console.log(response);
      setSuccess(response.data.message);
      const { token } = response.data;
      console.log(response.data.token);
      localStorage.setItem("token", token);

      router.push("/");
    } catch (error) {
      setSuccess(null);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }

      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(interval);
        } else {
          setSeconds(59);
          setMinutes(minutes - 1);
        }
      }
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [seconds]);
  return (
    // <div className="main-form">
    <div className="container-form">
      <div>
        <h2>Login with OTP</h2>
      </div>

      {otptrue ? (
        <div className="form1">
          <form onSubmit={handleVerifyOTP} className="form">
            <div className="text-field">
              <Form.Control
                style={{ width: "250px" }}
                onChange={(e) => setVerificationCode(e.target.value)}
                type="password"
                // id="outlined-basic"
                placeholder="Enter OTP..."
                // variant="outlined"
                value={verificationCode}
                required
              />
            </div>
            <div className="countdown-text">
              {seconds > 0 || minutes > 0 ? (
                <p>
                  Time Remaining: {minutes < 10 ? `0${minutes}` : minutes}:
                  {seconds < 10 ? `0${seconds}` : seconds}
                </p>
              ) : (
                <p>Didn't recieve code?</p>
              )}
            </div>
            {seconds > 0 || minutes > 0 ? (
              <Button
                type="submit"
                style={{
                  borderRadius: "50px",
                  backgroundColor: "#16252d",
                  border: "none",
                }}
                variant="primary"
              >
                Verify
              </Button>
            ) : (
              <Button
                onClick={handleSendOTP}
                style={{
                  borderRadius: "50px",
                  backgroundColor: "#16252d",
                  border: "none",
                }}
                variant="primary"
              >
                Resend OTP
              </Button>
            )}
          </form>
        </div>
      ) : (
        <div className="form1">
          <form onSubmit={handleSendOTP} className="form">
            <div className="text-field">
              <Form.Control
                style={{ width: "250px" }}
                onChange={(e) => setPhoneNumber(e.target.value)}
                // id="outlined-basic"
                placeholder="Enter your email or number..."
                // variant="outlined"
                value={phoneNumber}
                required
              />
            </div>
            <Button
              type="submit"
              style={{
                borderRadius: "50px",
                backgroundColor: "#16252d",
                border: "none",
              }}
              variant="primary"
            >
              Send OTP
            </Button>
          </form>
        </div>
      )}
    </div>
  );
};
// OtpLogin.getLayout = (page) => <>{page}</>;

export default OtpLogin;
