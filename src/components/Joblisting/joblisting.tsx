import React from "react";
import jobsData from "../../jsondata/joblisting.json";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";

type TextProps = {
  text: string;
  wordLimit: number;
};

function Text({ text, wordLimit }: TextProps) {
  const words = text.split(" ");
  const truncatedWords = words.slice(0, wordLimit);
  const truncatedText = truncatedWords.join(" ");

  return <p>{truncatedText}....</p>;
}

const Joblisting = () => {
  const router = useRouter();

  const handleJobDetails = (jobid: string) => {
    router.push(`/joblistings/${jobid}`);
  };

  return (
    <div className="joblisting-main-container">
      <div className="joblisting-container">
        <div className="joblisting-cards">
          {jobsData.jobs.map((job) => (
            <div className="joblisting-card-data" key={job.id}>
              <div className="joblisting-image">
                <img src={job.image} alt={job.title} />

                <h5>{job.title}</h5>
              </div>

              <Text text={job.description} wordLimit={20} />
              <div className="joblisting-card-bottom">
                <button onClick={() => handleJobDetails(job.id)}>
                  See details
                </button>
                <h6>{job.price}</h6>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Joblisting;
