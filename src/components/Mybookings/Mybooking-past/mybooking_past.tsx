import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import axios from "axios";

type Booking = {
  booking_id: any;
  booking_price: string;
  booking_start_time: string;
  booking_end_time: string;
  booking_time: string;
  booking_date: string;
  booking_service: string;
};

const Mybooking_past = () => {
  const [bookingsData, setBookingsData] = useState([]);
  const [selectedBooking, setSelectedBooking] = useState<Booking | null>(null);
  const [feedback, setFeedback] = useState<boolean>(false);
  const router = useRouter();

  useEffect(() => {
    const fetchBookingsData = async () => {
      try {
        const response = await axios.get("http://localhost:5000/bookings");

        const filteredBookingsData = response.data.filter(
          (booking: Booking) => {
            const bookingDate = new Date(booking.booking_date);
            const today = new Date();
            return bookingDate < today;
          }
        );

        const updatedBookingsData = filteredBookingsData.map(
          (booking: Booking) => {
            const dateSubstring = booking.booking_date.substring(0, 10);
            return { ...booking, booking_date: dateSubstring };
          }
        );

        setBookingsData(updatedBookingsData);
      } catch (error) {
        console.error(error);
      }
    };

    fetchBookingsData();
  }, []);
  const handleFeedback = (e: { preventDefault: () => void; stopPropagation: () => void; }) => {
    e.preventDefault();
    e.stopPropagation();
    console.log("feedback button")
    setFeedback(true);
    // router.push("/")
  };
  const handleClick = (booking: Booking) => {
    router.push({
      pathname: `/mybookings/${booking.booking_id}`,
      query: {
        date: booking.booking_date,
        time: `${booking.booking_start_time} - ${booking.booking_end_time}`,
        service: booking.booking_service,
        id: booking.booking_id,
        price: booking.booking_price,
      },
    });
  };
  return (
    <>
      {bookingsData.length === 0 ? (
        <div className="mybookings-upcoming">
          <div className="mybookings-upcoming-cards">
            <div className="mybookings-upcoming-cards-no-data">
              <h6>No Past Bookings</h6>
            </div>
          </div>
        </div>
      ) : !feedback ? (
        <div className="mybookings-past">
          <div className="mybookings-past-cards">
            {bookingsData.map((booking: Booking) => (
              <div
                key={booking.booking_id}
                className="mybooking-card"
                onClick={() => handleClick(booking)}
              >
                <div className="mybooking-card-service">
                  Booked For : {booking.booking_date}
                </div>
                <div className="mybooking-card-service">
                  Services Requested : {booking.booking_service}
                </div>

                <div className="mybooking-upcoming-footer">
                  <div className="mybooking-price">
                    ${booking.booking_price}
                  </div>
                  <div className="mybooking-card-time">
                    <button onClick={handleFeedback}>
                      Review
                      {/* {booking.booking_start_time}- {booking.booking_end_time} */}
                    </button>
                  </div>
                </div>
              </div>
            ))}
            
          </div>
        </div>
      ) : (
        <div  style={{zIndex:999}} className="mybookings-past">
          <div className="mybookings-past-cards">
            <h3 >Feedback</h3>
          </div>
        </div>
      )}
    </>
  );
};

export default Mybooking_past;
