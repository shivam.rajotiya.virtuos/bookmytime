export type Booking = {
    name: string;
    date: string;
    time: string;
    service: string;
    bookedfor: string;
    id: string;
    price: string;
    message: string;
  };
  