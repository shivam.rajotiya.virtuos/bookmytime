import React from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { useEffect, useState } from "react";

type Booking = {
  booking_id: any;
  booking_price: string;
  booking_start_time: string;
  booking_end_time: string;
  booking_time: string;
  booking_date: string;
  booking_service: string;
};

const Mybooking_upcoming = () => {
  const router = useRouter();
  const [selectedBooking, setSelectedBooking] = useState<Booking | null>(null);

  const [bookingsData, setBookingsData] = useState([]);
  console.log(bookingsData);
  useEffect(() => {
    const fetchBookingsData = async () => {
      try {
        const response = await axios.get("http://localhost:5000/bookings");

        const filteredBookingsData = response.data.filter(
          (booking: Booking) => {
            const bookingDate = new Date(booking.booking_date);
            const today = new Date();
            return bookingDate >= today;
          }
        );
        console.log(filteredBookingsData);
        const updatedBookingsData = filteredBookingsData.map(
          (booking: Booking) => {
            const dateSubstring = booking.booking_date.substring(0, 10);
            // const time1Substring = booking.booking_start_time.substring(0, 5);
            // const time2Substring = booking.booking_end_time.substring(0, 5);

            return { ...booking, booking_date: dateSubstring };

            // booking_start_time: time1Substring, booking_end_time: time2Substring};
          }
        );

        setBookingsData(updatedBookingsData);
        setBookingsData(filteredBookingsData);
      } catch (error) {
        console.error(error);
      }
    };

    fetchBookingsData();
  }, []);
  const handleClick = (booking: Booking) => {
    router.push({
      pathname: `/mybookings/${booking.booking_id}`,
      query: { 
        date: booking.booking_date,
        time: `${booking.booking_start_time} - ${booking.booking_end_time}`,
        service: booking.booking_service,
        id: booking.booking_id,
        price: booking.booking_price,
      },
    });
  };
  return (
    <>
      {bookingsData.length === 0 ? (
        <div className="mybookings-upcoming">
          <div className="mybookings-upcoming-cards">
            <div className="mybookings-upcoming-cards-no-data">
              <h6>No Upcoming Bookings</h6>
            </div>
          </div>
        </div>
      ) : (
        <div className="mybookings-upcoming">
          <div className="mybookings-upcoming-cards">
            {bookingsData.map((booking: Booking) => (
              // <Link
              //   href={`/mybookings/${booking.booking_id}`}
              //   key={booking.booking_id}
              // >
                <div key={booking.booking_id} className="mybooking-card"
                onClick={() => handleClick(booking)}
                >
                  <div className="mybooking-card-service">
                    Booked For : {booking.booking_date}
                  </div>
                  <div className="mybooking-card-service">
                    Services Requested : {booking.booking_service}
                  </div>

                  <div className="mybooking-upcoming-footer">
                    <div className="mybooking-price">
                      {booking.booking_price}
                    </div>
                    <div className="mybooking-card-time">
                      <button>
                        {booking.booking_start_time}- {booking.booking_end_time}
                      </button>
                    </div>
                  </div>
                </div>
              
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default Mybooking_upcoming;
