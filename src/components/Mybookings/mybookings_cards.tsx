import Mybookingtab from "@components/Tabs/MybookingsTab/mybookingtab";
import React from "react";

const Mybookings_cards = () => {
  return (
    <div className="mybookings-main-container">
      <div className="mybookings-container">
        <h1>Booking History</h1>
        <Mybookingtab />
      </div>
    </div>
  );
};

export default Mybookings_cards;
