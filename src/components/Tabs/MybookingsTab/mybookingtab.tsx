import Mybooking_upcoming from "@components/Mybookings/Mybooking-upcoming/mybooking_upcoming";
import Mybooking_past from "@components/Mybookings/Mybooking-past/mybooking_past";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import React from "react";

const Mybookingtab = () => {
  return (
    <Tabs >
      <TabList
        style={{
          display: "flex",
          justifyContent: "center",
          scrollbarWidth: "none",
          color: "blue",
        }}
      >
        <Tab style={{color: "#00a2fe",}}>Upcoming Bookings</Tab>
        <Tab style={{color: "#00a2fe",}}>Past Bookings</Tab>
      </TabList>

      <TabPanel >
        <Mybooking_upcoming />
      </TabPanel>

      <TabPanel >
        <Mybooking_past />
      </TabPanel>
    </Tabs>
  );
};

export default Mybookingtab;
