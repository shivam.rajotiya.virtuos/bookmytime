import React from "react";
import { Slide,Fade } from "react-awesome-reveal";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
const Dirworks = () => {
  return (
    <div className="dirworks">
      <div className="dirwork">
        <h3>How Thedir Works</h3>

        <div className="cards">
          {/* <Fade  delay={50} triggerOnce cascade> */}
        <Fade delay={250} triggerOnce >

        <div className="dirworks-container">
          <Carousel
            showThumbs={false}
            showArrows={false}
            autoPlay={true}
            stopOnHover={true}
            infiniteLoop={true}
          >
            <div className="dirworks-cards">
              {/* <div className="image">Image</div> */}
              <div className="dirworks-cards-caption">
                {/* <p>Furniture </p> */}
              </div>
            </div>
            <div className="dirworks-cards">
              {/* <div className="image">Image</div> */}
              <div className="dirworks-cards-caption">
                {/* <p>Furniture </p> */}
              </div>
            </div>
          </Carousel>
        </div>
        </Fade>
          
          {/* </Fade> */}
        </div>
      </div>
    </div>
  );
};

export default Dirworks;
