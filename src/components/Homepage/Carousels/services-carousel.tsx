import React from "react";
import { Fade } from "react-awesome-reveal";

import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
const Services_carousel = () => {
  return (
    <div className="services">
      <div className="service">
        <Fade delay={50} triggerOnce>
          <h3>Popular Services</h3>
          <div className="service-container">
            <Carousel
              showThumbs={false}
              showArrows={false}
              autoPlay={true}
              stopOnHover={true}
              infiniteLoop={true}
            >
              <div className="service-cards">
                {/* <div className="image">Image</div> */}
                <div className="service-cards-caption">
                  {/* <p>Furniture </p> */}
                </div>
              </div>
              <div className="service-cards">
                {/* <div className="image">Image</div> */}
                <div className="service-cards-caption">
                  {/* <p>Furniture </p> */}
                </div>
              </div>
            </Carousel>
          </div>
        </Fade>
      </div>
    </div>
  );
};

export default Services_carousel;
