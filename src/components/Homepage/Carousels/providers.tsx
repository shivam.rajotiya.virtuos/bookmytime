import React from "react";
import { Carousel } from "react-responsive-carousel";
import { Fade, Slide } from "react-awesome-reveal";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import Link from "next/link";
const Providers = () => {
  return (
    <div className="providers-main-conatiner">
      <div className="provider">
        <h3>The Right Providers</h3>
        <div className="provider-wrap">
          <Slide delay={200} triggerOnce>
            <Carousel
              showThumbs={false}
              infiniteLoop={true}
              autoPlay={true}
              stopOnHover={true}
              showArrows={false}
            >
              <div className="provider-cards-container">
                <h6>Engineer</h6>

                <div className="provider-cards-column">
                  {/* <Carousel
                  showThumbs={false}
                  infiniteLoop={true}
                  autoPlay={true}
                  stopOnHover={true}
                > */}
                  <div className="provider-cards">
                    <div className="image"></div>
                    {/* <div className="provider-cards-caption">
                </div> */}
                  </div>
                  {/* <div className="provider-cards">
                    <div className="image"></div> */}
                  {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                  {/* </div> */}
                  {/* <div className="provider-cards">
                    <div className="image"></div> */}
                  {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                  {/* </div> */}
                  {/* </Carousel> */}
                </div>
              </div>
              <div className="provider-cards-container">
                <h6>Lawyer</h6>
                <div className="provider-cards-column">
                  {/* <Carousel
                  showThumbs={false}
                  stopOnHover={true}
                  infiniteLoop={true}
                  autoPlay={true}
                > */}
                  <div className="provider-cards">
                    <div className="image">Image</div>
                    {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                    {/* </div>
                  <div className="provider-cards">
                    <div className="image">Image</div> */}
                    {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                // </div> */}
                  </div>
                  {/* <div className="provider-cards">
                    <div className="image">Image</div>
                    {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                  {/* </div> */}
                  {/* </Carousel> */}
                </div>
              </div>
              <div className="provider-cards-container">
                <h6>Doctor</h6>
                <div className="provider-cards-column">
                  {/* <Carousel
                  showThumbs={false}
                  stopOnHover={true}
                  infiniteLoop={true}
                  autoPlay={true}
                > */}
                  <div className="provider-cards">
                    <div className="image">Image</div>
                    {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                  </div>
                  {/* <div className="provider-cards">
                    <div className="image">Image</div>
                    <div className="provider-cards-caption">
                      <p>Furniture </p>
                    </div>
                  </div>
                  <div className="provider-cards">
                    <div className="image">Image</div>
                    {/* <div className="provider-cards-caption">
                  <p>Furniture </p>
                </div> */}
                  {/* </div> */}
                  {/* </Carousel> */}
                </div>
              </div>
            </Carousel>
          </Slide>
        </div>
        {/* <div className="provider-cards-container">
          <h6>Home Improvements</h6>
          <div className="provider-cards-column">
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
          </div>
        </div>
        <div className="provider-cards-container">
          <h6>Home Improvements</h6>
          <div className="provider-cards-column">
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
          </div>
        </div>
        <div className="provider-cards-container">
          <h6>Home Improvements</h6>
          <div className="provider-cards-column">
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
            <div className="provider-cards">
              <div className="image">Image</div>
              <div className="provider-cards-caption">
                <p>Furniture </p>
              </div>
            </div>
          </div>
        </div>
      // </div> */}
      </div>
    </div>
  );
};

export default Providers;
