import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import Link from "next/link";
const Footer_home = () => {
  return (
    <div className="homepage-footer-main-container">
      {/* <div className="footer"> */}
      <div className="footer-container">
        <div className="footer-cont">
          <div className="footer-columns">
            <div className="footer-column">
              <h5>Company</h5>
              <div className="footer-text">
                <Link href={"/aboutus"}>About us</Link>
                <Link href={"/privacypolicy"}>Privacy Policy</Link>
                <Link href={"/termsandconditions"}>Terms & Conditions</Link>
                <Link href={"/faq"}>FAQ</Link>
                {/* <Link href={"/aboutus"}>About us</Link>
                <Link href={"/aboutus"}>About us</Link> */}
              </div>
            </div>
            <div className="footer-column">
              <h5>Customer</h5>
              <div className="footer-text">
                <Link href={"/categories"}>Categories</Link>
                <Link href={"/contactus"}>Contact us</Link>
                <Link href={"/blog"}>Blog</Link>
              </div>
            </div>
            <div className="footer-column">
              <h5>Service Partner</h5>
              <div className="footer-text">
                <Link href={"/register"}>Register as Provider</Link>
              </div>
            </div>

            <div className="footer-column">
              <h5>Newsletter</h5>
              <div className="footer-text">
                <p>
                  Sign up for our newsletter to receive updates on our latest
                  offerings, including appointments with engineers, lawyers,
                  doctors, and other professionals. Stay informed and never miss
                  out on an opportunity to get the services you need!
                </p>
                <div className="search">
                  <input type="search" placeholder="Email Address..." />
                  <button>GO</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyrights">
        <p>&copy;Virtuos 2023 All Rights Reserved</p>
        <div className="footer-icons">
          <InstagramIcon />
          <FacebookIcon />
          <TwitterIcon />
        </div>
      </div>
    </div>
  );
};

export default Footer_home;
