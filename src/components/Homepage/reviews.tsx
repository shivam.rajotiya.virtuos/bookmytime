import BasicRating from "@components/Ratings/rating";
import React from "react";
import Avatar from "@mui/material/Avatar";
import { Fade } from "react-awesome-reveal";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import reviewsData from "../../jsondata/review.json";
const Reviews = () => {
  return (
    <div className="reviews-main-container">
      <div className="reviews-container">
        <h3>Customer Feedback </h3>

        <div className="reviews">
          <div className="review-cards">
            <Carousel
              showThumbs={false}
              infiniteLoop={true}
              autoPlay={true}
              stopOnHover={true}
            >
              {reviewsData.reviews.map((review) => (
                <div className="review" key={review.id}>
                  <div className="ratings">
                    <div className="avatar">
                      <Avatar
                        alt="Remy Sharp"
                        src="https://media.npr.org/assets/img/2022/11/08/ap22312071681283-0d9c328f69a7c7f15320e8750d6ea447532dff66.jpg"
                      />
                    </div>
                    <div className="customer">
                      <div className="customer-name">{review.user.name}</div>
                      <div className="rating">
                        <BasicRating />
                      </div>
                    </div>
                  </div>
                  <div className="review-body">
                    <div className="review-text">
                      <h6>{review.title}</h6>
                      <p>{review.comment}</p>
                      <p>Date: {review.date}</p>
                    </div>
                  </div>
                </div>
              ))}
            </Carousel>
          </div>
        </div>

        <span className="review-link">
          <p>
            Read more of what people are saying about BookMyTime on{" "}
            <a href="#">BookMyTime.com</a>
          </p>
        </span>
      </div>
    </div>
  );
};

export default Reviews;
