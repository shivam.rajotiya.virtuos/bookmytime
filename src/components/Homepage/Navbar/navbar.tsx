import React from "react";
import AddLocationIcon from "@mui/icons-material/AddLocation";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import Sidemenu from "../../SideMenu/side_menu";
import SearchIcon from '@mui/icons-material/Search';
import Navbar_header from "./nav_header";
const Navbar = () => {
  return (
    <div className="navbar-main-container">
      <div className="navbar-container">
        <div className="navbar-header">
          <div className="logo">
            <div className="ham-menu">
                <Sidemenu/>
            </div>
            <AddLocationIcon />
            <h5>thedir</h5>
          </div>
          <div className="navmenu">
            < Navbar_header/>
            <p>Explore</p>
            <p>Blogs</p>

          </div>
          <div className="nav-login">
            <AccountCircleIcon />
            <p>Log in</p>
          </div>
        </div>
        <div className="navbar-body">
          <h1>Find services professional for </h1>
          <h1>whatever you need</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis,
            rerum?
          </p>
        </div>

        <div className="nav-search">
            <p>WHAT SERVICES DO YOU NEED?</p>
            <div className="search">
            <input className="nav-searchbox" type="search" placeholder="" />
            <button>
                <SearchIcon/>

            </button>
            </div>
            
        
        </div>
      </div>
    </div>
  );
};

export default Navbar;
