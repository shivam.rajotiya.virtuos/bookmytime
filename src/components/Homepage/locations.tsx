import React from "react";
import statesData from "../../jsondata/location.json";
const Locations = () => {
  const states = statesData.states;
  const columnSize = Math.ceil(states.length / 3);

  const columns = [
    states.slice(0, columnSize),
    states.slice(columnSize, columnSize * 2),
    states.slice(columnSize * 2),
  ];
  return (
    <div className="locations-main-container">
      <div className="location-container">
        <h3>BookMyTime is Covering The Whole India </h3>
        <div className="location-name-container">
          <div className="state-name" style={{ display: "flex" }}>
            {columns.map((column, index) => (
              <div className="location-row" key={index} style={{ flex: 1 }}>
                {column.map((state) => (
                  <div className="state" key={state}>{state}</div>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Locations;
