import React from "react";
import { Fade, Slide } from "react-awesome-reveal";

const Learnmore = () => {
  return (
    <div className="learnmore-main-container">
      <div className="learnmore-container">
        <Fade delay={500} triggerOnce>
          <div className="learnmore-text">
            <h3>Are you a local service professional?</h3>
            <h6>Learn how BookMyTime can help double your business</h6>
          </div>
          <div className="learnmore-button">
            <button>Learn More </button>
          </div>
        </Fade>
      </div>
    </div>
  );
};

export default Learnmore;
