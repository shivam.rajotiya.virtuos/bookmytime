import React from "react";
import CountUp from "react-countup";
import {  Fade } from "react-awesome-reveal";

const Registered_user = () => {
  return (
    <div className="registered-user-main-container">
      <div className="registered-container">
      <Fade delay={500} triggerOnce>

        <div className="registered-user">

          <div className="registered">
            <CountUp
              start={-2345}
              duration={30.75}
              decimal=","
              end={65468543}
              delay={0}
            >
              {({ countUpRef }) => (
                <div className="counter">
                  <span ref={countUpRef} />
                </div>
              )}
            </CountUp>
            <p>Registered Users</p>
          </div>
          <div className="registered">
            <CountUp
              start={-23470}
              duration={20.75}
              decimal=","
              end={496595}
              delay={0}
            >
              {({ countUpRef }) => (
                <div className="counter">
                  <span ref={countUpRef} />
                </div>
              )}
            </CountUp>
            <p>Providers</p>
          </div>
        </div>
        {/* <div className="download-button">
          <h5>App Store</h5>
          <h5>Google Play</h5>
        </div> */}

        {/* </div> */}
        </Fade>
      </div>
    </div>
  );
};

export default Registered_user;
