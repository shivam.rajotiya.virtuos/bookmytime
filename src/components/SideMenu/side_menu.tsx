import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import MenuIcon from "@mui/icons-material/Menu";
import Link from "next/link";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import sidemenu from "./sidemenu.json";
type Anchor = "left";

export default function Sidemenu() {
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === "keydown" &&
        ((event as React.KeyboardEvent).key === "Tab" ||
          (event as React.KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const list = (anchor: Anchor) => (
    <Box
      sx={{ width: 280 }}
      role="presentation"
      style={{
        background: "#16252d",
        color: "white",
        height: "100vh",
      }}
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List style={{ marginTop: "2rem", marginLeft: "2rem" }}>
        <div className="sidemenu-options">
          {sidemenu.map((option) => (
            <Link href={option.url} key={option.id}>
              <div key={option.id} className="sidemenu-option">
                {/* <span className="option-icon"></span> */}
                <span className="option-text">{option.title}</span>
                <span>
                  <KeyboardArrowRightIcon />
                </span>
              </div>
            </Link>
          ))}
        </div>
      </List>
    </Box>
  );

  return (
    <div>
      {(["left"] as const).map((anchor) => (
        <React.Fragment key={anchor}>
          <Button
            style={{ color: "white" }}
            onClick={toggleDrawer(anchor, true)}
          >
            <MenuIcon style={{ color: "white" }} />
          </Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
