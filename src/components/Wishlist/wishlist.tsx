import React from "react";
import swal from "sweetalert";

import wishlistdata from "./wishlistdata.json";
import DeleteIcon from "@mui/icons-material/Delete";
const Wishlist = () => {
  const handleDelete = () => {
    swal({
      title: "Item removed from Wishlist",

      icon: "success",
    });
  };

  return (
    <div className="cart-main-container">
      <div className="cart-container">
        <div className="wishlist-header">
          <div className="wishlist-title">
            <h1>Wishlist</h1>
          </div>
        </div>

        <div className="cart-body">
          {wishlistdata.map((data) => (
            <div key={data.id} className="cart-card">
              <div className="wishlist-card-service">
                <h6>Service : {data.service}</h6>
                <p>{data.message}</p>
              </div>
              {/* <div className="wishlist-card-comments">{data.message}</div> */}
              <div className="wishlist-card-footer">
                <div className="cart-price">{data.price}</div>
                {/* <div className="wishlist-card-service">{data.date}</div> */}

                {/* <div className="cart-card-time">
                  <button>{data.time}</button>
                </div> */}
                <div className="wishlist-addtocart-button">
                  <button>Move to Cart</button>
                  <DeleteIcon onClick={handleDelete} />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Wishlist;
