import * as React from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import FolderIcon from "@mui/icons-material/Folder";
import RestoreIcon from "@mui/icons-material/Restore";
import FavoriteIcon from "@mui/icons-material/Favorite";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import HomeIcon from "@mui/icons-material/Home";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
// import FavoriteIcon from '@mui/icons-material/Favorite';
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import Link from "next/link";
export default function LabelBottomNavigation() {
  const [value, setValue] = React.useState("");

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
    console.log(newValue);
  };

  return (
    <BottomNavigation
      style={{
        background: "linear-gradient(to right, #16252d, #228788)",
      }}
      sx={{ width: "100%" }}
      value={value}
      onChange={handleChange}
    >
      <BottomNavigationAction
        style={{ color: "white" }}
        label="Home"
        value="home"
        icon={
          <Link href="/">
            <HomeIcon />
          </Link>
        }
      />
      <BottomNavigationAction
        style={{ color: "white" }}
        label="MyBookings"
        value="bookings"
        icon={
          <Link href="/mybookings">
            <CalendarMonthIcon />
          </Link>
        }
      />

      <BottomNavigationAction
        style={{ color: "white" }}
        label="Wishlist"
        value="wishlist"
        icon={
          <Link href={"/wishlist"}>
            <FavoriteIcon />
          </Link>
        }
      />

      <BottomNavigationAction
        style={{ color: "white" }}
        label="Cart"
        value="cart"
        icon={
          <Link href="/cart">
            <ShoppingCartIcon />
          </Link>
        }
      />

      <BottomNavigationAction
        style={{ color: "white" }}
        label="Account"
        value="account"
        icon={
          <Link href="/account">
            <ManageAccountsIcon />
          </Link>
        }
      />
    </BottomNavigation>
  );
}
