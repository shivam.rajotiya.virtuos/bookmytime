import Messages from "@components/Messages/message";
import React from "react";

const messages = () => {
  return (
    <div>
      <Messages />
    </div>
  );
};

export default messages;
