import React from "react";
import App from "next/app";
import { SSRProvider } from "react-bootstrap";
import { useRouter } from "next/router";

// import your global styles and layouts here
import "@styles/globals.scss";
import "@styles/login.css";
import "@styles/home.css";
import Layout from "@components/Layout/layout";
import { PageWithLayout } from "../types/PageWithLayout";
import withAuth from "src/middleware/withAuth";

function MyApp({ Component, pageProps }: any) {
  const router = useRouter();

  // exclude login and register pages from requiring authentication
  const excludedRoutes = ["/login", "/register"];
  const isExcludedRoute = excludedRoutes.includes(router.pathname);



  
 
  // Apply withAuth to the component only if it is not an excluded route
  const AuthComponent = isExcludedRoute ? Component : withAuth(Component);

  // Use getLayout function to wrap component with layout
  const getLayout =
    (Component as PageWithLayout).getLayout ||
    ((page: JSX.Element) => <Layout children={page} />);

  return (
    <SSRProvider>
      {getLayout(<AuthComponent {...pageProps} />)}
    </SSRProvider>
  );
}

export default MyApp;
