import React from "react";
import notificationsData from "../jsondata/notification.json";
const Notifications = () => {
  return (
    <div className="notification-main-container">
      <div className="notification-container">
        <h1>User Notifications</h1>
        <div className="notification-messages">
          {notificationsData.notifications.map((notification) => (
            <div className="notification-message-card" key={notification.booking_id}>
              <p>{notification.message}</p>
             <span> <p >{new Date(notification.time).toLocaleString('en-GB', {
              day: 'numeric',
              month: 'long',
              hour: 'numeric',
              minute: 'numeric',
              hour12: false
            })}</p>
            </span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Notifications;
