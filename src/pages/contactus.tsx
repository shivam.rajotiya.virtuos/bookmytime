import Image from "next/image";
import React, { useState } from "react";
import swal from "sweetalert";
import contact from "./images/contact.jpg";
const Contact_us = () => {
  const [email, setEmail] = useState<string | number>("");
  const [subject, setSubject] = useState<string | number>("");
  const [message, setMessage] = useState<string | number>("");

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    setEmail("");
    setSubject("");
    setMessage("");
    console.log(email, subject, message);

    if (email === "" || subject === "" || message === "") {
      swal("Error", "Please fill all the fields", "error");
    } else {
      swal("Success", "Thank you for contacting us!", "success");
    }
    // swal({
    //   title: "Thank you for contacting us!",

    //   icon: "success",
    // });
  };
  return (
    <div className="contactus-main-container">
      <div className="contactus-container">
        <div className="contactus-text-image">
          <Image src={contact} alt="contact" />
          <div className="contact-text">
            <h1>Contact Us</h1>
            <p>
              Welcome to our Contact Us page! We’re always happy to hear from
              our users and customers. If you have any questions about our
              products or services, please feel free to reach out to us. Our
              customer support team is available to help you with any queries
              you may have. You can contact us through the form provided on this
              page or you can <a href="mailto:email@example.com"> click here</a>{" "}
              to write an email. Our team typically responds within 24 hours. If
              you have any feedback or suggestions for how we can improve our
              services, please let us know. We’re constantly striving to make
              our products and services better, and we value your input. We also
              welcome any business inquiries or partnership opportunities. If
              you’re interested in working with us, please don’t hesitate to
              reach out. Thank you for choosing our website. We look forward to
              hearing from you!
            </p>
          </div>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="contactus-form">
            <div className="contact-form-desktop">
              <div className="form-group">
                <input
                  type="email"
                  value={email}
                  // required
                  onChange={(e) => setEmail(e.target.value)}
                  className="form-control"
                  placeholder="Email"
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  // required
                  value={subject}
                  onChange={(e) => setSubject(e.target.value)}
                  className="form-control"
                  placeholder="Subject"
                />
              </div>
            </div>

            <div className="form-group-textfield">
              <textarea
                className="form-control"
                // required
                value={message}
                placeholder="Message"
                onChange={(e) => setMessage(e.target.value)}
              ></textarea>
            </div>
            <button type="submit" className="contact-us-button">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Contact_us;
