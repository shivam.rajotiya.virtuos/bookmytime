import { useContext } from "react";
// import { AuthContext } from "../contexts/AuthContext";
// import ProtectedRoute from "../components/ProtectedRoute";

import LabelBottomNavigation from "@components/BottomNavbar/bottom_fixed_navbar";
import Providers from "@components/Homepage/Carousels/providers";
import Services_carousel from "@components/Homepage/Carousels/services-carousel";
import Footer_home from "@components/Homepage/Footer/footer";
import Dirworks from "@components/Homepage/dirworks";
import Learnmore from "@components/Homepage/learnmore";
import Locations from "@components/Homepage/locations";
import Registered_user from "@components/Homepage/registered_user";
import Reviews from "@components/Homepage/reviews";
import Trustedby from "@components/Homepage/trustedby";
import { Fade, Slide } from "react-awesome-reveal";
import React from "react";
import Navbar from "@components/Bookings/Navbar/navbar";
import withAuth from "src/middleware/withAuth";
import Calendly from "@components/calendly/calendly";
import Joblisting from "@components/Joblisting/joblisting";

interface ProtectedPageProps {}

const homepage: React.FC<ProtectedPageProps> = (props) => {
  return (
    <>
      <Joblisting />
      <Services_carousel />
      <Providers />
      <Reviews />
      <Learnmore />
      <Locations />
      <Registered_user />
      {/* <Trustedby /> */}
      <Calendly />
    </>
  );
};
export default withAuth(homepage);
