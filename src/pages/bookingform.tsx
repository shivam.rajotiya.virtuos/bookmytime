import { Form } from "react-bootstrap";
import { useRouter } from "next/router";
import { Dayjs } from "dayjs";
import swal from "sweetalert";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { useState } from "react";
const axios = require("axios");
import { TextField } from "@mui/material";
type Job = {
  id: string;
  title: string;
  category: string;
  image: string;
  description: string;
  link: string;
  price: string;
};
const BookingForm = (): JSX.Element => {
  const router = useRouter();
  // console.log("bookingForm");
  const { job } = router.query;
  const [phoneNumber, setphoneNumber] = useState<string | number>("");
  const [name, setName] = useState<string | number>("");
  const [address, setAddress] = useState<string | number>("");
  const [city, setCity] = useState<string | number>("");
  const [state, setState] = useState<string | number>("");
  const [pincode, setPincode] = useState<string | number>("");
  const [details, setDetails] = useState<boolean>(true);
  const [checkout, setCheckout] = useState<boolean>(true);
  const [date, setDate] = useState<Dayjs | null>(null);
  const [timestart, setTimestart] = useState<Dayjs | null>(null);
  const [timeend, setTimeend] = useState<Dayjs | null>(null);
  const handleCheckout = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    console.log(`Date: ${date?.format("YYYY-MM-DD")}`);
    console.log(`Starting Time: ${timestart?.format("HH:mm A")}`);
    console.log(`End Time: ${timeend?.format("HH:mm A")}`);
    setCheckout(false);
  };
  
  const handleConfirm = (e: { preventDefault: () => void }) => {
    e.preventDefault();

    try {
      axios
        .post("http://localhost:5000/bookings", {
          booking_description: parsedJob.category,
          booking_price: parsedJob.price,
          booking_date: `${date?.format("YYYY-MM-DD")}`,
          booking_start_time: `${timestart?.format("HH:mm A")}`,
          booking_end_time: `${timeend?.format("HH:mm A")}`,
          booking_service: parsedJob.title,
        })
        .then((response: { data: any }) => {
          console.log(response.data);
        });
    } catch (error) {
      console.log(error);
    }
    swal({
      title: "Bookings Confirmed",
      icon: "success",
    });
    setTimeout(() => {
      router.push("/mybookings");
    }, 3000);
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setDetails(false);

    console.log(phoneNumber, name, address);
  };
  const parsedJob: Job = job
    ? JSON.parse(job as string)
    : {
        id: "",
        title: "",
        category: "",
        image: "",
        description: "",
        link: "",
        price: "",
      };
  return (
    <>
      {!parsedJob ? (
        <p>Loading...</p>
      ) : (
        <div className="booking-form-main-container">
          {details ? (
            <div className="booking-form-cont">
              <h3>Complete your booking</h3>
              <div className="booking-form">
                <div className="booking-form-details">
                  <div className="booking-form-details-text">
                    <div className="joblisting-image">
                      <img src={parsedJob.image} alt={parsedJob.title} />
                      <div className="joblisting-image-section">
                        <h6>{parsedJob.category}</h6>
                        <p>{parsedJob.price}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <form onSubmit={handleSubmit} className="form">
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      type="text"
                      placeholder="Name"
                      onChange={(e) => setName(e.target.value)}
                      value={name}
                      required
                    />
                  </div>
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      placeholder="Mobile number"
                      onChange={(e) => setphoneNumber(e.target.value)}
                      value={phoneNumber}
                      required
                    />
                  </div>
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      type="address"
                      onChange={(e) => setAddress(e.target.value)}
                      value={address}
                      placeholder="Address"
                      required
                    />
                  </div>
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      type="address"
                      onChange={(e) => setCity(e.target.value)}
                      value={city}
                      placeholder="City"
                      required
                    />
                  </div>
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      type="address"
                      onChange={(e) => setState(e.target.value)}
                      value={state}
                      placeholder="State"
                      required
                    />
                  </div>
                  <div className="text-field">
                    <Form.Control
                      style={{ width: "250px" }}
                      type="address"
                      onChange={(e) => setPincode(e.target.value)}
                      value={pincode}
                      placeholder="Pincode"
                      required
                    />
                  </div>
                  <button type="submit">Continue</button>
                </form>
              </div>
            </div>
          ) : (
            <div className="booking-form-cont">
              {!checkout ? (
                <>
                  {!parsedJob.price ? (
                    <p>loading..</p>
                  ) : (
                    <div className="checkout-main-container">
                      <div className="checkout-container">
                        <h3>Checkout Page</h3>
                        <h6>Service Selected:{parsedJob.title}</h6>
                        <p>
                          Selected Date:{" "}
                          {date
                            ? date.format("YYYY-MM-DD")
                            : "No date selected"}
                        </p>
                        <p>
                          Time Slot:{" "}
                          {timestart ? timestart.format("HH:mm A") : "-"}-
                          {timeend ? timeend.format("HH:mm A") : "-"}
                        </p>
                        <h6>Price: {parsedJob.price}</h6>
                        <button onClick={handleConfirm}>Confirm</button>
                      </div>
                    </div>
                  )}
                </>
              ) : (
                <>
                  <h3>Choose Date and Slot</h3>
                  <div className="booking-form-date-main">
                    <div className="booking-form-date">
                      <form className="form" onSubmit={handleCheckout}>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <div>
                            <DatePicker
                              label="Pick Date for Booking"
                              value={date}
                              disablePast
                              onChange={(newDate: Dayjs | null) =>
                                setDate(newDate)
                              }
                              // textField={(params: JSX.IntrinsicAttributes) => (
                              //   <TextField {...params} />
                              // )}
                            />
                          </div>
                          <div className="booking-form-time">
                            <div className="booking-form-time-input-field">
                              <TimePicker
                                label="Starting Time"
                                value={timestart}
                                onChange={(newTime: Dayjs | null) =>
                                  setTimestart(newTime)
                                }
                                // textField={(
                                //   params: JSX.IntrinsicAttributes
                                // ) => <TextField {...params} />}
                              />
                            </div>
                            <h4>-</h4>
                            <div className="booking-form-time-input-field">
                              <TimePicker
                                label="End Time"
                                value={timeend}
                                onChange={(newTime: Dayjs | null) =>
                                  setTimeend(newTime)
                                }
                                // textField={(
                                //   params: JSX.IntrinsicAttributes
                                // ) => <TextField {...params} />}
                              />
                            </div>
                          </div>
                        </LocalizationProvider>
                        <div>
                          <button type="submit">Checkout</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </>
              )}
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default BookingForm;
