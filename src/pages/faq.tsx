import React from "react";
import faqdata from "../jsondata/faqdata.json";
const Faq = () => {
  return (
    <div className="faq-main-container">
      <div className="faq-container">
        <div className="faq-title">FAQ</div>
        {faqdata.map((data) => (
          <div className="faq-item">
            <div className="faq-item-title">{data.question}</div>
            <div className="faq-item-content">
              <p>{data.answer}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Faq;
