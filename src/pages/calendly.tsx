import React from 'react';
import { InlineWidget } from 'react-calendly';

const Calendly: React.FC = () => {
  return (
    <div className="inline-widget">
      <InlineWidget url="https://calendly.com/virtuos-bookmytime/" />
    </div>
  );
};

export default Calendly;
