import React from "react";

const Termsandconditions = () => {
  return (
    <div className="terms-and-conditions-main-container">
      <div className="terms-and-conditions-container">
        <h1>Terms & Conditions</h1>

        <p>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam, id
          quisquam ex eos provident quibusdam deleniti nam et at minus eum
          reiciendis beatae error consequatur maxime porro cumque, veritatis
          fuga cum hic sequi eius quas! Aspernatur repellendus asperiores
          tempora totam? Vitae excepturi unde, possimus reprehenderit ut ipsam
          expedita? Libero ea sunt labore laudantium beatae reiciendis obcaecati
          ducimus accusamus. Nesciunt perferendis laudantium hic quod fugit
          voluptates atque necessitatibus corporis unde accusantium. Neque
          perspiciatis tenetur atque cumque quos impedit ea enim facilis
          distinctio minus. Repellat inventore illum pariatur vel ex provident
          possimus. Lorem ipsum dolor sit amet consectetur, adipisicing elit.
          Placeat soluta esse vel natus minima asperiores ipsa, et deleniti
          consequatur amet? Lorem ipsum dolor sit amet consectetur adipisicing
          elit. Obcaecati excepturi doloremque aliquam molestias quae, ea odit
          adipisci omnis hic provident.
        </p>
      </div>
    </div>
  );
};

export default Termsandconditions;
