import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";

const editprofile = () => {
  const handleSubmit = () => {};
  const [username, setUsername] = useState<string | number | string[] | undefined>("");
// const name = localStorage.getItem("name")
// setUsername(name)
  return (
    <div className="editprofile-main-container">
      <div className="editprofile-container">
        <h2>Edit your details!</h2>
        <div className="edit-details-form">
          <form onSubmit={handleSubmit} className="form">
            <div className="text-field">
              <Form.Control
                style={{ width: "250px" }}
                onChange={(e) => setUsername(e.target.value)}
                type="username"
                // id="outlined-basic"
                placeholder="Enter your new username..."
                // variant="outlined"
                value={username}
                required
              />
              
            </div>
            <Button
                type="submit"
                style={{
                  borderRadius: "50px",
                  backgroundColor: "#16252d",
                  border: "none",
                }}
                variant="primary"
              >
                Submit
              </Button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default editprofile;
