import { useRouter } from "next/router";

// type{ bookingsData =
//     name: string;
//     date: string;
//     time: string;
//     service: string;
//     bookedfor: string;
//     id: string;
//     price: string;
//     message: string;
  


const BookingDetailsPage = (): JSX.Element => {
  const router = useRouter();

  const { name, date, time, service, id, price } = router.query;

  if (!id) {
    return <div>Booking not found</div>;
  }

  return (
    <div className="booking-details">
      <h1>Booking Details</h1>
      <div className="booking-details-cont">
        
        <div>Date: {date}</div>
        <div>Time: {time}</div>
        <div>Service: {service}</div>
        <div>Price: {price}</div>
      </div>
    </div>
  );
};

export default BookingDetailsPage;
