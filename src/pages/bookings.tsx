import LabelBottomNavigation from "@components/BottomNavbar/bottom_fixed_navbar";
import Footer_home from "@components/Homepage/Footer/footer";
import Booking_cards from "@components/Bookings/Bookingcards/booking_cards";
import Navbar from "@components/Bookings/Navbar/navbar";
import React from "react";

const Bookings = () => {
  return (
    <>
      <Booking_cards />
    </>
  );
};

export default Bookings;
