import React from 'react'

const Privacypolicy = () => {
  return (
    <div className="terms-and-conditions-main-container">
      <div className="terms-and-conditions-container">
        <h1>Privacy Policy</h1>

        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem
          asperiores dolore eligendi veniam laborum totam expedita, eius fuga,
          quibusdam recusandae alias odit cum ratione ipsam laboriosam similique
          tempora quisquam eos numquam, voluptate assumenda voluptas. Vel
          suscipit hic debitis quos quas, natus, reiciendis soluta dolores
          facilis minus unde est voluptatem adipisci neque sed, eaque officiis.
          Ipsa laborum aspernatur velit reprehenderit fugit odit quos quod
          repudiandae voluptas et a officia inventore consequatur nobis, quas,
          enim minima accusantium voluptatibus eaque eveniet veniam fugiat.
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi
          molestiae ipsum, cupiditate tenetur ducimus nisi dolor asperiores
          nesciunt numquam itaque non dolorem ratione magni veniam?
        </p>
      </div>
    </div>
  )
}

export default Privacypolicy