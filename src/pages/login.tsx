import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Image from "next/image";
import { useRouter } from "next/router";
import axios from "axios";
import { PageWithLayout } from "../types/PageWithLayout";
import bodyimg from "./images/book-my-time-img.svg";
import virtuoslogo from "./images/virtuos-virtuez-logo.svg";
import headerlogo from "./images/book-my-time-logo.svg";
import Link from "next/link";
import { NextPage } from "next";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";
import OtpLogin from "@components/Otplogin/otplogin";
const Login: PageWithLayout = () => {
  const router = useRouter();

  const [email, setEmail] = useState<string | number>("");
  const [password, setPassword] = useState<string | number>("");
  const [error, setError] = useState<unknown | any>(null);
  const [show, setShow] = useState<boolean>(false);
  const [otplogin, setOtplogin] = useState<boolean>(false);
  const handleSubmit = async (event: { preventDefault: () => void }) => {
    event.preventDefault();

    try {
      const response = await axios.post("http://localhost:5000/auth/login", {
        "user_email": email,
        "user_password": password,
      });
      const { access_token, useremail, name } = response.data;
      console.log(response);
      console.log(response.data.access_token);
      localStorage.setItem("token", access_token);
      localStorage.setItem("useremail", useremail);
      localStorage.setItem("name", name);
      router.push("/");
    } catch (error) {
      setError("Invalid email or password.");
    }
    setEmail("");
    setPassword("");
  };
  const handleOtplogin = () => {
    setOtplogin(true);
  };
  return (
    <div className="main-form">
      <div className="header-form">
        <Image src={headerlogo} alt="bookmytime-logo" />
      </div>

      <div className="body-form">
        <div className="byt-img">
          <Image src={bodyimg} alt="bookmytime-img" />
        </div>
        {!otplogin ? (
          <div className="container-form">
            <div>
              <h2>Welcome</h2>
            </div>
            <div className="form1">
              <form onSubmit={handleSubmit} className="form">
                <div className="text-field">
                  <Form.Control
                    style={{ width: "250px" }}
                    onChange={(e) => setEmail(e.target.value)}
                    type="email"
                    // id="outlined-basic"
                    placeholder="Enter your Email"
                    // variant="outlined"
                    value={email}
                    required
                  />
                </div>

                <div className="text-field">
                  <Form.Control
                    style={{ width: "250px" }}
                    onChange={(e) => setPassword(e.target.value)}
                    type={show ? "text" : "password"}
                    // id="outlined-basic"
                    placeholder="Enter your Password"
                    // variant="outlined"
                    value={password}
                    required
                  />
                  {password ? (
                    <span className="hide-show" onClick={() => setShow(!show)}>
                      {show ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </span>
                  ) : null}
                </div>

                <Button
                  type="submit"
                  style={{
                    borderRadius: "50px",
                    backgroundColor: "#16252d",
                    border: "none",
                  }}
                  variant="primary"
                >
                  Login
                </Button>
              </form>
              <div className="login-links">
                <Link style={{ color: "#16252d" }} href="/register">
                  Dont have an account? Register
                </Link>
                <div
                  style={{
                    textDecoration: "underline",
                    color: "#16252d",
                    cursor: "pointer",
                  }}
                  onClick={handleOtplogin}
                >
                  Login with OTP
                </div>
              </div>
              {error && <p>{error}</p>}
            </div>
          </div>
        ) : (
          <OtpLogin />
        )}
      </div>

      <div className="footer">
        <div className="v-logo">
          <Image src={virtuoslogo} alt="Virtuos-Logo" />
        </div>
        <div className="copy">
          © Virtuos Digital Ltd. Virtuez Assimilations. All rights reserved.
        </div>
      </div>
    </div>
  );
};
Login.getLayout = (page) => <>{page}</>;

export default Login;
