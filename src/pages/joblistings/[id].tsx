import { Button } from "react-bootstrap";
import jobsData from "../../jsondata/joblisting.json";
import { useRouter } from "next/router";

const JobDetailsPage = (): JSX.Element => {
  const router = useRouter();
  const { id } = router.query;
  type Job = {
    id: string;
    title: string;
    category: string;
    image: string;
    description: string;
    link: string;
    price:string | number;
  };
  const job: Job | undefined = jobsData.jobs.find((job) => job.id === id);

  if (!job) {
    return <div>Job not found</div>;
  }
  const query = { job: JSON.stringify(job) };
  const handleBookingClick = () => {
    router.push({ pathname: '/bookingform', query });
  };
  return (
    <div className="job-details-main-container">
      <div className="job-details-cont">
        <h3>{job.title}</h3>
        <div className="job-details">
          <div className="job-detail-img">
            <img src={job.image} alt={job.title} />
          </div>
          <div className="job-details-text">
            <h6>Category: {job.category}</h6>
            <p>Description: {job.description}</p>
            <button onClick={handleBookingClick}>Book Now</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default JobDetailsPage;
