import teamData from "../jsondata/aboutus.json";

interface TeamMember {
  name: string;
  profession: string;
  experience: number;
  specialization: string;
}

interface BookingSystem {
  steps: string[];
  features: string[];
}

interface TeamData {
  title: string;
  description: string;
  team: TeamMember[];
  booking_system: BookingSystem;
  feedback: string;
}

function Aboutus() {
  const { title, description, team, booking_system, feedback }: TeamData =
    teamData;

  return (
    <div className="about-us-main-container">
      <div className="about-us-container">
        <h1>{title}</h1>
        <p>{description}</p>

        <h2>Our Team</h2>
        <div className="member-card">
          {team.map((member: TeamMember, index: number) => (
            <div className="members" key={index}>
              <h3>{member.name}</h3>
              <p>{member.profession}</p>
              <p>{member.specialization}</p>
              <p>{member.experience} years of experience</p>
            </div>
          ))}
        </div>
        <div className="booking-system">
          <h2>Booking System</h2>
          <ul>
            {booking_system.steps.map((step: string, index: number) => (
              <li key={index}>{step}</li>
            ))}
          </ul>
          <ul>
            {booking_system.features.map((feature: string, index: number) => (
              <li key={index}>{feature}</li>
            ))}
          </ul>
        </div>
        <p>{feedback}</p>
      </div>
    </div>
  );
}

export default Aboutus;
