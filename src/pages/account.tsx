import React from "react";
import Avatar from "@mui/material/Avatar";
import profiledata from "../components/SideMenu/sidemenu.json";
import Link from "next/link";
import EditIcon from "@mui/icons-material/Edit";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";

const Account = () => {
  // if (typeof window !== "undefined") {
  // const name = localStorage.getItem("name");
  // const email = localStorage.getItem("useremail");
  // }
  return (
    <div className="account-main-container">
      <div className="account-container">
        <div className="account-avatar-section">
          <Avatar
            style={{ height: "4rem", width: "4rem" }}
            alt="Remy Sharp"
            src="https://media.npr.org/assets/img/2022/11/08/ap22312071681283-0d9c328f69a7c7f15320e8750d6ea447532dff66.jpg"
          />
          <div className="account-name-email">
            <h5>Israel Lavi</h5>
            <h6>israel.lavi@gmail.com</h6>
          </div>
          <Link href={"/editprofile"}>
            <div className="edit-account">
              <EditIcon />
            </div>
          </Link>
        </div>

        <div className="account-info-section">
          <div className="account-info">
            {profiledata.map((option) => (
              <Link href={option.url} key={option.id}>
                <div key={option.id} className="account-option">
                  <span className="account-text">{option.title}</span>
                  <span>
                    <KeyboardArrowRightIcon />
                  </span>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Account;
